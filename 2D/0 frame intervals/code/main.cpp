//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.10+
//

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Scene.hpp"
#include "Engine.hpp"

using namespace sf;
using namespace example;

int main ()
{
    // Se crea una ventana en la que mostrar la animación:

    RenderWindow window(sf::VideoMode(800, 600), "Frame Intervals");

    // Se crea una escena que lleva a cabo la animación:

    Scene scene;

    // Se crea, configura e inicia el engine, que controla la ejecución:

    Engine & engine = Engine::instance ();

    engine.set (window);
    engine.set (scene );

    engine.execute ();

    return 0;
}
