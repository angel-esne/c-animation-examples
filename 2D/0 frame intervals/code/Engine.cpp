//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.10+
//

#include "Scene.hpp"
#include "Engine.hpp"

using namespace sf;

namespace example
{

    void Engine::run ()
    {
        // Se inicializa el cronómetro de fotogramas:

        Clock timer;
        float time_estimation = 1.f / 60.f;

        // Se ejecuta el bucle principal del engine:

        running = true;

        do
        {
            timer.restart ();

            // Se leen los eventos encolados y se pasan a la escena activa para que los procese:

            if (scene)
            {
                Event event;

                while (window->pollEvent (event))
                {
                    scene->handle (event);
                }
            }

            // Se pide a la escena activa que actualice su estado:

            if (scene)
            {
                // Se actualiza la escena según el tiempo estimado:

                scene->update (time_estimation);
            }

            // Se realiza el proceso de render estándar con doble buffer:

            window->clear (Color::Black);           // Se limpia el buffer oculto

            if (scene)
            {
                scene->render ();                   // Se dibuja la escena en el buffer oculto
            }

            window->display ();                     // Se intercambia el buffer oculto con el visible
                                                    // Puede tardar un tiempo si está activo el VSync

            // Se estima la duración del siguiente fotograma asumiendo que será similar a la duración
            // del actual:

            time_estimation = timer.getElapsedTime ().asSeconds ();
        }
        while (running);
    }

}
