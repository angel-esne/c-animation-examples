//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.10+
//

#include "Scene.hpp"
#include "Engine.hpp"

#include <iostream>

using namespace sf;

namespace example
{

    Scene::Scene()
    :
        circle  (50),
        position(300.f, 300.f),
        speed   (275.f, 250.f)
    {
        circle.setFillColor (Color::Blue);
    }

    void Scene::handle (const Event & event)
    {
        // Sólo se procesa el evento 'Closed'. El resto de eventos se ignoran.

        switch (event.type)
        {
            case Event::Closed:
            {
                Engine::instance ().terminate ();
                break;
            }
        }
    }

    void Scene::update (float estimated_time)
    {
        float    diameter   = circle. getRadius () * 2.f;
        Vector2u windowSize = window->getSize   ();
        Vector2f limit      = Vector2f(float(windowSize.x), float(windowSize.y));

        // Se calcula la nueva posición del círculo en función del tiempo estimado:

        position += speed * estimated_time;

        // Se ajusta la nueva posición si se sale de los límites de la ventana:

        if (position.x <  0.f)
        {
            position.x =  0.f;
            speed.x   *= -1.f;
        }

        if (position.y <  0.f)
        {
            position.y =  0.f;
            speed.y   *= -1.f;
        }

        if (position.x +  diameter > limit.x)
        {
            position.x =  limit.x - diameter;
            speed.x   *= -1.f;
        }

        if (position.y +  diameter > limit.y)
        {
            position.y =  limit.y - diameter;
            speed.y   *= -1.f;
        }

        // Se establece en el círculo la nueva posición ajustada:

        circle.setPosition (position);
    }

    void Scene::render ()
    {
        RenderTarget * renderer = dynamic_cast< RenderTarget * >(window);

        if (renderer)
        {
            renderer->draw (circle);
        }
    }

}
