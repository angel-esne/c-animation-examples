//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.10+
//

#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace example
{

    class Scene
    {

        typedef sf::RenderWindow Window;

        sf::CircleShape circle;
        sf::Vector2f    position;
        sf::Vector2f    speed;
        sf::Window    * window;

    public:

        /** Inicializa los elementos de la escena.
          */
        Scene();

        void set (Window & new_window)
        {
            window = &new_window;
        }

        /** Se encarga de tratar los eventos.
          */
        void handle (const sf::Event & event);

        /** Actualiza el estado de la animación.
          */
        void update (float time);

        /** Dibuja un fotograma en la ventana.
          */
        void render ();

    };

}
