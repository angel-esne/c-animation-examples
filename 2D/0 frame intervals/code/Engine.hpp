//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.10+
//

#pragma once

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace example
{

    class Scene;        // Declaración adelantada de la clase Scene.

    class Engine
    {

        typedef sf::RenderWindow Window;

    public:

        /** Este método permite acceder desde cualquier lugar a la instancia única de Engine.
          */
        static Engine & instance ()
        {
            static Engine engine;
            return engine;
        }

    private:

        Window * window;                ///< Puntero a la ventana de la aplicación
        Scene  * scene;                 ///< Puntero a la escena activa
        bool     running;               ///< Este flag controla la continuidad del bucle principal

    private:

        Engine()
        {
            window  = nullptr;
            scene   = nullptr;
            running = false;
        }

        void run ();

    public:

        void set (Window & new_window)
        {
            window = &new_window;

            if (scene) scene->set (*window);
        }

        void set (Scene & new_scene)
        {
            scene = &new_scene;

            scene->set (*window);
        }

        /// Inicia el bucle principal del engine.

        void execute ()
        {
            if (running == false)   // Se evita iniciarlo si ya está iniciado
            {
                run ();
            }
        }

        /// Desactiva el flag que controla la salida del bucle principal para detenerlo.
        /// El fotograma en curso se completará.

        void terminate ()
        {
            running = false;
        }

    };

}
