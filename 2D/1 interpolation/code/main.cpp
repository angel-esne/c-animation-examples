//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2015.11+
//

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Linear_Interpolator.hpp"

using namespace sf;
using namespace example;

void draw_point (RenderWindow & window, float x, float y, Color color = Color::White)
{
    CircleShape circle(2);

    circle.setFillColor (color);
    circle.setPosition  (x, y);

    window.draw (circle);
}

template< size_t DIMENSION, typename NUMERIC_TYPE >
void draw_interpolation (RenderWindow & window, Interpolator< DIMENSION, NUMERIC_TYPE > & interpolator, Color color = Color::White)
{
    // Se dibujan los puntos interpolados en el rango [0, 1):

    for(float t = 0.f; t < 1.f; t += 0.025f)
    {
        Point2f sample = interpolator.sample (t);

        draw_point (window, sample[0], sample[1], color);
    }

    // Se dibuja el último punto interpolado en t = 1:

    Point2f sample = interpolator.sample (1.f);

    draw_point (window, sample[0], sample[1], color);
}

int main ()
{
    RenderWindow window(VideoMode(800, 600), "Interpolation", sf::Style::Default, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

    Point2f origin = make_point2f (100, 100);
    Point2f ending = make_point2f (700, 500);

    Linear_Interpolator< 2, float > linear_interpolator(origin, ending);

    bool running = true;

    do
    {
        // Process window events:

        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Render:

        window.clear ();

        draw_interpolation (window, linear_interpolator, Color::Blue);

        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
