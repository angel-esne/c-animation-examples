
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
*                                                                             *
*  Started by �ngel on november of 2015                                       *
*                                                                             *
*  This is free software released into the public domain.                     *
*                                                                             *
*  angel.rodriguez@esne.edu                                                   *
*                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef DF_SPRITE_HEADER
#define DF_SPRITE_HEADER

    #include "SpriteHandler.hpp"

    namespace sample
    {

        class DF_Sprite
        {
        private:

            AnimationInfo animation_info;       ///< Datos de las animaciones
            SpriteHandler sprite_handler;       ///< Manejador de los datos de un sprite
            sf::Texture   sprite_texture;       ///< Textura del sprite

        public:

            DF_Sprite(const std::string & assets_path, const std::string asset_name)
            {
                pugi::xml_document animation_data;
                pugi::xml_document    sprite_data;

                animation_data.load_file ((assets_path + asset_name).c_str ());
                   sprite_data.load_file ((assets_path + animation_info.loadSheetName (animation_data)).c_str ());

                animation_info.loadFromXmls     (animation_data, sprite_data);
                sprite_texture.loadFromFile     (assets_path + animation_info.getImageName ());
                animation_info.setTexture       (sprite_texture);
                sprite_handler.setAnimationInfo (animation_info);
            }

        public:

            /// Retorna el objeto transform del sprite.
            ///
            sf::Transform & transform ()
            {
                return sprite_handler.transform ();
            }

            /// Establece una animaci�n a partir de su �ndice.
            //
            void set_animation (size_t index)
            {
                sprite_handler.setAnim (int(index));
            }

            /// Actualiza el estado de la animaci�n activa.
            //
            void update ()
            {
                sprite_handler.update ();
            }

            /// Dibuja el sprite seg�n la animaci�n y el fotograma activos.
            ///
            void render (sf::RenderWindow & window)
            {
                sprite_handler.draw (window);
            }

        };

    }

#endif
