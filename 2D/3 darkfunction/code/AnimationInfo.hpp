
// Copyright (c) 2014 Gatleos
// All rights reserved
// https://github.com/Gatleos/sfml_darkfunction_loader

#ifndef ANIMATIONINFO_H
#define ANIMATIONINFO_H

    #include "pugixml.hpp"
    #include <SFML\Graphics.hpp>

    class AnimationInfo
    {
    public:

        struct Sprite
        {
            sf::IntRect draw;
            sf::Transform transform;
            Sprite(){}
            Sprite(const Sprite &s)
            {
                draw = s.draw;
                transform = s.transform;
            }
        };

        struct Cell
        {
            unsigned int delay;
            std::multimap<int, Sprite> sprites;
        };

        struct Animation
        {
            std::string name;
            unsigned int loops;
            std::vector<Cell> cells;
        };

    private:

        std::string imagename;//Name of image to draw sprites from
        std::string sheetname;//Name of .sprite file
        sf ::Texture *tx;//Image to draw sprites from
        std::vector<Animation> animations;//List of all animations

    public:

        std::string loadSheetName(pugi::xml_document &ani)//Load the name of the spritesheet from a .anim; required before calling load()!
        {
            pugi::xml_node node = ani.child("animations");
            sheetname = node.attribute("spriteSheet").value();
            return sheetname;
        }

        std::string getImageName()//Return the name of the image
        {
            return imagename;
        }

        std::string getSheetName()//Return the name of the spritesheet
        {
            return sheetname;
        }

        sf::Texture * getTexture ()
        {
            return tx;
        }

        Animation & getAnimation (int num)
        {
            return animations[num];
        }

        void loadFromXmls(pugi::xml_document &ani, pugi::xml_document &spr);//Load all animation data

        void setTexture(sf::Texture &t)//Set the texture pointer to any sf::texture
        {
            tx = &t;
        }

    };//AnimationInfo

#endif
