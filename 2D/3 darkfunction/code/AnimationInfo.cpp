
// Copyright (c) 2014 Gatleos
// All rights reserved
// https://github.com/Gatleos/sfml_darkfunction_loader

#include "AnimationInfo.hpp"

void AnimationInfo::loadFromXmls(pugi::xml_document &ani, pugi::xml_document &spr)//Load all animation data
{
    imagename = spr.first_child().first_attribute().value();
    pugi::xml_node node = ani.child("animations");

    for(node = node.first_child(); node; node = node.next_sibling())
    {
        animations.push_back(Animation());
        Animation &a = animations[animations.size() - 1];
        a.name = node.attribute("name").value();
        a.loops = node.attribute("loops").as_uint();

        for(pugi::xml_node node2 = node.first_child(); node2; node2 = node2.next_sibling())
        {
            a.cells.push_back(Cell());
            Cell &c = a.cells[a.cells.size() - 1];
            c.delay = node2.attribute("delay").as_uint();

            for(pugi::xml_node node3 = node2.first_child(); node3; node3 = node3.next_sibling())
            {
                int z = node3.attribute("z").as_int();
                std::pair<int, Sprite> smap(z, Sprite());
                Sprite &s = smap.second;
                std::string name = node3.attribute("name").value();
                std::string folder;
                pugi::xml_node spinfo = spr.first_child().first_child();
                int pos = 0;

                for(auto it = name.begin(); it != name.end(); it++, pos++)
                {
                    if(*it == '/')
                    {
                        if(!folder.size())
                            folder = "/";

                        for(pugi::xml_node_iterator it = spinfo.begin(); it != spinfo.end(); it++)
                        {
                            if(it->first_attribute().value() == folder)
                            {
                                spinfo = *it;
                                break;
                            }
                        }
                        folder.clear();
                    }
                    else
                    {
                        folder.push_back(*it);
                    }
                }

                for(pugi::xml_node_iterator it = spinfo.begin(); it != spinfo.end(); it++)
                {
                    if(it->first_attribute().value() == folder)
                    {
                        spinfo = *it;
                        break;
                    }
                }

                s.draw = sf::IntRect(spinfo.attribute("x").as_int(), spinfo.attribute("y").as_int(), spinfo.attribute("w").as_int(), spinfo.attribute("h").as_int());
                s.transform.translate(node3.attribute("x").as_float() - (float(s.draw.width) / 2.f), node3.attribute("y").as_float() - (float(s.draw.height) / 2.f));
                s.transform.rotate(node3.attribute("angle").as_float(), float(s.draw.width) / 2.f, float(s.draw.height) / 2.f);

                if(node3.attribute("flipH").as_bool())
                {
                    s.draw.left += s.draw.width;
                    s.draw.width *= -1;
                }

                if(node3.attribute("flipV").as_bool())
                {
                    s.draw.top += s.draw.height;
                    s.draw.height *= -1;
                }

                c.sprites.insert(smap);
            }
        }
    }
}
