
// Copyright (c) 2014 Gatleos
// All rights reserved
// https://github.com/Gatleos/sfml_darkfunction_loader

#ifndef SPRITEHANDLER_H
#define SPRITEHANDLER_H

    #include "AnimationInfo.hpp"

    class SpriteHandler
    {
    private:

        AnimationInfo *animinfo;
        sf::Sprite sp;
        sf::Transform t;
        AnimationInfo::Animation *current_anim;
        AnimationInfo::Cell *current_cell;
        int ccell;

    public:

        sf::Transform & transform ()
        {
            return t;
        }

        void update()//Update the current frame (must be called at least once before draw()!)
        {
            ccell++;
            if (ccell >= (int)current_anim->cells.size() || ccell < 0)
            {
                ccell = 0;
            }
            current_cell = &current_anim->cells[ccell];
        }

        void draw(sf::RenderTarget &r)//Render the current frame to the screen
        {
            for (auto it = current_cell->sprites.rbegin(); it != current_cell->sprites.rend(); ++it)
            {
                sp.setTextureRect(it->second.draw);
                r.draw(sp, t * it->second.transform);
            }
        }

        void setAnimationInfo(AnimationInfo &ai)
        {
            animinfo = &ai;
            sp.setTexture(*animinfo->getTexture ());
        }

        void setAnim(int num)
        {
            current_anim = &animinfo->getAnimation (num);
            ccell = -1;
        }

        void setCell(int num)
        {
            current_cell = &current_anim->cells[num];
            ccell = num;
        }

    };//SpriteHandler

#endif
