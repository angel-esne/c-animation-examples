
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Started by Ángel on november of 2015                                       *
 *                                                                             *
 *  This is free software released into the public domain.                     *
 *                                                                             *
 *  angel.rodriguez@esne.edu                                                   *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "DF_Sprite.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;
using namespace sample;

string asset_path (const string & name)
{
    return string().append (name);
}

int main ()
{
    RenderWindow window(VideoMode(800, 600), "darkFunction", Style::Default, ContextSettings(32));

    window.setVerticalSyncEnabled (true);

    // Se crea un sprite a partir de los datos de animación de un archivo de darkFunction:

    DF_Sprite sprite("../../assets/", "heavyducker.anim");

    // Se inicializa el sprite:

    sprite.set_animation (0);
    sprite.transform ().translate (400.f, 250.f);

    // Se reproduce una animación en bucle:

    bool running = true;

    do
    {
        // Se procesan los eventos de la ventana:

        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Se actualiza y se dibuja el sprite:

        window.clear   ();
        sprite.update  ();
        sprite.render  (window);
        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
