//
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2021.04+
//

#include <ciso646>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <Cube.hpp>
#include <Light.hpp>
#include <Model.hpp>
#include <OpenGL.hpp>
#include <Model_Obj.hpp>
#include <Render_Node.hpp>

using namespace glt;
using namespace std;


namespace
{

    unique_ptr< Render_Node > create_scene ()
    {
        // Se crean los elementos (nodos) y la escena a la que se añadirán:

        unique_ptr< Render_Node > scene (new Render_Node);
        shared_ptr< Model       > model (new Model_Obj("../../assets/bunny.obj"));
        shared_ptr< Model       > cube  (new Model);
        shared_ptr< Camera      > camera(new Camera(20.f, 1.f, 500.f, 1.f));
        shared_ptr< Light       > light (new Light);

        // Es necesario añadir las mallas a los modelos antes de añadir los modelos a la escena:

        cube->add (shared_ptr< glt::Drawable >(new Cube), Material::default_material ());

        // Se añaden los nodos a la escena:

        scene->add ("model" , model );
        scene->add ("cube"  , cube  );
        scene->add ("camera", camera);
        scene->add ("light" , light );

        return scene;
    }

    void configure_scene (Render_Node & scene)
    {
        scene["light"]->translate (Vector3(10.f, 10.f, 10.f));
        scene["model"]->translate (Vector3( 0.f,  0.f, -5.f));
        scene["cube" ]->translate (Vector3( 0.f, -1.f, -5.f));
        scene["cube" ]->scale     (5.f, 0.1f, 5.f);
    }

    void reset_viewport (const sf::Window & window, Render_Node & scene)
    {
        GLsizei width  = GLsizei(window.getSize ().x);
        GLsizei height = GLsizei(window.getSize ().y);

        scene.get_active_camera ()->set_aspect_ratio (float(width) / height);

        glViewport (0, 0, width, height);
    }

}


int main ()
{
    sf::RenderWindow window
    (
        sf::VideoMode(1024, 768), 
        "Simple Scene", 
        sf::Style::Default, 
        sf::ContextSettings(24, 0, 0, 3, 2, sf::ContextSettings::Core)      // Se usa OpenGL 3.2 core profile
    );

    window.setVerticalSyncEnabled (true);
    
    // Se determinan las características de OpenGL disponibles en la máquina:

	if (not glt::initialize_opengl_extensions ())
	{
		exit (-1);
	}
    
    // Se crea y se configura la escena:

    auto scene = create_scene ();

    Node * model = scene->get ("model");

    model->set_transformation (glm::mat4(1));

    configure_scene (*scene);

    // Se inicializan algunos elementos de OpenGL:

    reset_viewport (window, *scene);

    glEnable     (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.2f, 1.f);

    bool running = true;

    do
    {
        sf::Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                {
                    running = false;
                    break;
                }

                case sf::Event::Resized:
                {
                    reset_viewport (window, *scene);
                    break;
                }
            }
        }

        model->rotate_around_y (0.01f);

        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        scene->render ();

        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
